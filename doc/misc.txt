COMMENT:
MISCellaneous-Functions
-----------------------

NAME:
	complete_complete_plet
SYNOPSIS:
	INT complete_complete_plet(OP a, OP b, OP c)
DESCRIPTION:
	Computes the plethysm of two complete symmetric functions labeled
	by INTEGER objects. The first parameter is the outer one, so we
	compute h_a(h_b). The result is a SCHUR object.
EXAMPLE:
	#include <symmetrica.h>
	ANFANG
	scan(INTEGER,a);
	scan(INTEGER,b);
	complete_complete_plet(a,b,c);
	println(c);
	ENDE

NAME:
	complete_schur_plet
SYNOPSIS:
	INT complete_schur_plet(OP a, OP b, OP c)
DESCRIPTION:
	Computes the plethysm of a complete symmetric function labeled by
	the INTEGER object a and the Schurfunction labeled by the INTEGER
	or PARTITION object b. The result will be a SCHUR function c. The
	complete function is the outer one, so we compute h_a(s_b)
EXAMPLE
	#include <symmetrica.h>
	ANFANG
	scan(INTEGER,a);
	scan(PARTITION,b);
	complete_schur_plet(a,b,c);
	println(c);
	ENDE

NAME:
	elementary_schur_plet
SYNOPSIS:
	INT elementary_schur_plet(OP a, OP b, OP c)
DESCRIPTION:
	Computes the plethysm of a elementary symmetric function labeled by
	the INTEGER object a and the Schurfunction labeled by the INTEGER
	or PARTITION object b. The result will be a SCHUR function c. The
	elementary function is the outer one, so we compute e_a(s_b).
EXAMPLE:
	#include <symmetrica.h>
	ANFANG
	scan(INTEGER,a);
	scan(PARTITION,b);
	elementary_schur_plet(a,b,c);
	println(c);
	ENDE

NAME:
	power_schur_plet
SYNOPSIS:
	INT power_schur_plet(OP a, OP b, OP c)
DESCRIPTION:
	Computes the plethysm of a power symmetric function labeled by
	the INTEGER object a and the Schurfunction labeled by the INTEGER
	or PARTITION object b. The result will be a SCHUR function c. The
	complete function is the outer one, so we compute p_a(s_b).
EXAMPLE:
	#include <symmetrica.h>
	ANFANG
	scan(INTEGER,a);
	scan(PARTITION,b);
	power_schur_plet(a,b,c);
	println(c);
	ENDE

NAME:
	schur_schur_plet
SYNOPSIS:
	INT schur_schur_plet(OP a, OP b, OP c)
DESCRIPTION:
	Computes the plethysm of two Schurfunctions labeled by a PARTITION
	object or by an INTEGER object. The first parameter is the outer one,
	so we compute s_a(s_b). The result is a SCHUR object.
EXAMPLE:
	#include <symmetrica.h>
	ANFANG
	scan(PARTITION,a);
	scan(PARTITION,b);
	schur_schur_plet(a,b,c);
	println(c);
	ENDE
