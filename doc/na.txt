
NAME:
	ndg
SYNOPSIS:
	INT ndg(OP part, OP perm, OP res)
DESCRIPTION:
	computes integral irreducible representation labeled by the
	PARTITION object part for the PERMUTATION
	object perm. The result will be a MATRIX object.
EXAMPLE:
        #include <symmetrica.h>

        SYM_BEGIN
        scan(scanobjectkind(),a);
        scan(PERMUTATION,b);
        ndg(a,b,c);
        println(c);
        SYM_END


